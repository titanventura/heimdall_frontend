const video = document.getElementById("video");
const isScreenSmall = window.matchMedia("(max-width: 700px)");
var can_snap = true;

/****Loading the model ****/
let user_not_found_count = 0;
Promise.all([
    faceapi.nets.ssdMobilenetv1.loadFromUri("/static/models"),
]).then(startVideo);

function startVideo() {
    navigator.getUserMedia(
        {video: {}},
        stream => (video.srcObject = stream),
        err => console.error(err)
    );
}

/****Fixing the video with based on size size  ****/
function screenResize(isScreenSmall) {
    if (isScreenSmall.matches) {
        video.style.width = "360px";
    } else {
        video.style.width = "500px";
    }
}

screenResize(isScreenSmall);
isScreenSmall.addListener(screenResize);

video.addEventListener('play', async () => {
    const canvas = faceapi.createCanvasFromMedia(video)
    document.getElementById("containerr").append(canvas)
    const displaySize = {width: video.width, height: video.height}
    faceapi.matchDimensions(canvas, displaySize)
    while (true) {
        // To Detect all faces.
        // const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions());

        // To detect single face.
        const detection = await faceapi.detectSingleFace(video, new faceapi.TinyFaceDetectorOptions());

        if (detection != null && can_snap == true) {
            // Should be only here.. not anywhere else
            const resizedDetections = faceapi.resizeResults(detection, displaySize);

            // const blob = canvas.toDataURL("image/png");

            const imageObj = new Image()
            const context = canvas.getContext("2d")
            context.drawImage(video, 0, 0, video.width, video.height)

            var src = "";
            src = canvas.toDataURL("image/jpeg")
            imageObj.src = src;

            var block = src.split(";")
            var contentType = block[0].split(":")[1]
            var realData = block[1].split(",")[1]
            var blob = b64toBlob(realData, "png")
            console.log(detection);
            can_snap = false


            checkIn(blob).then(res => {
                if (res.user != null) {
                    const box = resizedDetections._box
                    const drawBox = new faceapi.draw.DrawBox(box, {
                        label: res.user,
                        boxColor: "green",
                        lineWidth: 5,
                        drawLabelOptions: {fontSize: 25}
                    })
                    drawBox.draw(canvas)
                }
                can_snap = true
            })
            canvas.getContext('2d').clearRect(0, 0, video.width, video.height);
        }
    }
})

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

async function rfid_detect() {
    var rfid_value = document.getElementById('rfid_value').value;
    var form_data = new FormData();
    var entry = false;
    if (window.location.href.includes('check_in')) {
        entry = true;
        console.log('entry');
    } else if (window.location.href.includes('check_out')) {
        console.log('exit');
        entry = false;
    }
    form_data.append("entry", entry);
    form_data.append("rfid", true);
    form_data.append("rfid_value", rfid_value);
    var requestOptions = {
        "url": `http://localhost:8000/api/check_in_or_out/`,
        "method": 'POST',
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form_data
    };
    var result = await $.ajax(requestOptions).done(function (res) {
        console.log(res)
    })
}

async function checkIn(blob) {
    var form_data = new FormData();
    var entry = false;
    if (window.location.href.includes('check_in')) {
        entry = true;
        console.log('entry');
    } else if (window.location.href.includes('check_out')) {
        console.log('exit');
        entry = false;
    }
    form_data.append("entry", entry);
    form_data.append("rfid", false);
    form_data.append("image_1", blob);
    form_data.append("token",token);
    for (var i of form_data.entries()) {
        console.log(i[0] + " " + i[1]);
    }
    var requestOptions = {
        "url": "/api/check_in_or_out/",
        "method": 'POST',
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form_data
    };
    var result = await $.ajax(requestOptions).done(function (res) {
        console.log(res)
    })
    response = JSON.parse(result);
    if (response.user != null) {
        document.getElementById("user").textContent = capitalize(response.user)
        stopStreamedVideo(video)
        swal(response.message).then(() => {
            window.location.reload()
        })

    } else {
        document.getElementById("user").textContent = capitalize(response.message)
        user_not_found_count++
        if (user_not_found_count > 5) {
            stopStreamedVideo(video)
            swal(response.message).then(() => {
                window.location.reload()
            })
        }
    }
    setTimeout(() => {
        document.getElementById("user").textContent = " ";
    }, 1000)
    console.log("canvas count", document.getElementsByName("canvas").length);
    return response
}


async function postImage(blob) {

    var form_data = new FormData();
    const myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic c3JpbmF0aDpzcm50aHNyZGhybg==");


    form_data.append("image_1", blob);
    for (var i of form_data.entries()) {
        console.log(i[0] + " " + i[1]);
    }
    var requestOptions = {
        "url": `${url_1}/predict_user/`,
        "method": 'POST',
        "timeout": 0,
        "headers": {
            "Authorization": "Basic c3JpbmF0aDpzcm50aHNyZGhybg==",
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form_data
    };
    var result = await $.ajax(requestOptions).done(function (res) {
        console.log(res)
    })

    response = JSON.parse(result)

    if (response.user != null) {
        var entry = false
        console.log(window.location.href)
        if (window.location.href.includes('check_in')) {
            entry = true
            console.log('entry')
        } else if (window.location.href.includes('check_out')) {
            console.log('exit')
            entry = false
        }
        const json_resp = check_in_or_out(response.user, entry);

        console.log(json_resp)

        document.getElementById("user").textContent = capitalize(response.user)
    } else {

        document.getElementById("user").textContent = capitalize(response.message)
    }
    setTimeout(() => {
        document.getElementById("user").textContent = " ";
    }, 1000)
    console.log("canvas count", document.getElementsByName("canvas").length);
    return response;

}

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}


function stopStreamedVideo(videoElem) {
    const stream = videoElem.srcObject;
    const tracks = stream.getTracks();

    tracks.forEach(function (track) {
        track.stop();
    });

    videoElem.srcObject = null;
}

const check_in_or_out = async (user, entry) => {
    const resp = await fetch('/face_recognition/check_in_and_out/', {
        method: "POST",
        body: JSON.stringify({
            'username': user,
            "entry": entry
        })
    })

    return await resp.json();
}
