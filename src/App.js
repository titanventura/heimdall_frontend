import React, { useState } from 'react';
import UserDetailForm from "./components/user_detail_form"
import VideoComponent from "./components/video"


function App() {

  const [torender, setToRender] = useState(false)

  return (
    <div className="App">
      <UserDetailForm setVideoRender={(value) => { setToRender(value) }} />
      {torender ? <VideoComponent /> : null}
    </div>
  );
}

export default App;
