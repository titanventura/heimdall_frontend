import React, { useEffect } from "react"
// import { tf } from '@tensorflow/tfjs-node';

import * as canvas from 'canvas';

import * as faceapi from 'face-api.js';

class VideoComponent extends React.Component {


    constructor(props) {
        super(props)
        this.video_ref = React.createRef()
        this.canvas_ref = React.createRef()
        this.MODELURL = "./face-api/models"

    }

    componentDidMount = () => {
        this.streamCamVideo()
    }

    useFaceApi = async () => {
        const video = this.video_ref.current
        const canvas = this.canvas_ref.current

        const displaySize = { width: video.width, height: video.height }
        faceapi.matchDimensions(canvas, displaySize)
        setInterval(async () => {
            const detections = await faceapi.detectSingleFace(video, new faceapi.TinyFaceDetectorOptions());
            console.log(detections)

        }, 200)
    }


    streamCamVideo = () => {
        var constraints = { audio: true, video: { width: 1280, height: 720 } };
        navigator.mediaDevices
            .getUserMedia(constraints)
            .then((mediaStream) => {
                var video = this.video_ref.current

                video.srcObject = mediaStream;
                video.onloadedmetadata = async (e) => {
                    video.play()
                    await faceapi.nets.tinyFaceDetector.loadFromUri(this.MODELURL)
                    this.useFaceApi()
                }
            })
            .catch(function (err) {
                console.log(err.name + ": " + err.message);
            }); // always check for errors at the end.
    }

    render() {
        return (
            <div>
                <video width="500" height="500" ref={this.video_ref}></video>
                <canvas width="500" height="500" ref={this.canvas_ref}></canvas>
            </div>
        )
    }

}

export default VideoComponent